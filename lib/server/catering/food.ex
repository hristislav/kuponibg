defmodule Server.Catering.Food do
  use Ecto.Schema
  import Ecto.Changeset

  @all_fields ~w(food_type name price description catering_id)a

  schema "foods" do
    field :food_type
    field :name, :string
    field :price, :float
    field :description, :string
    belongs_to :catering, Server.Accounts.Catering
    has_many :menu, Server.Catering.Menu
    # many_to_many :menus, Server.Catering.Menu, join_through: "menus_foods"

    timestamps()
  end

  @doc false
  def changeset(food, attrs) do
    food
    |> cast(attrs, @all_fields)
    |> validate_required(@all_fields)
  end
end

