defmodule Server.Catering.FoodType do
  use Ecto.Schema
  import Ecto.Changeset
  schema "food_types" do
    field :name, :string
    timestamps()
  end

  @doc false
  def changeset(food_type, attrs) do
    food_type
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
