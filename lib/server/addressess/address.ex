defmodule Server.Addressess.Address do
  use Ecto.Schema
  import Ecto.Changeset

  schema "addressess" do
    field :address, :string
    field :city, :string
    field :country, :string
    field :post_code, :string
    field :province, :string

    has_one :parent, Server.Accounts.Parent
    has_one :school, Server.Accounts.School
    has_one :catering, Server.Accounts.Catering
    has_one :host, Server.Accounts.Host

    timestamps()
  end

  @doc false

  # def changeset(address, attrs) do
  #   address
  #   |> cast(attrs, [:country, :city, :province, :address, :post_code])
  #   |> validate_required([:country, :city, :province, :address, :post_code])
  # end

  def changeset(address, attrs) do
    address
    |> cast(attrs, [:city,:country,:province,:address,:post_code])
    |> validate_required([:city])
  end
end
