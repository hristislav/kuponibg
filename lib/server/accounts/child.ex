defmodule Server.Accounts.Child do
  use Ecto.Schema
  import Ecto.Changeset
  schema "children" do
    field :name, :string
    field :rfid
    field :secure_code
    field :secure_code_confirmation, :string, virtual: true
    field :egn
    field :conf_code, :string
    field :active?, :boolean, default: false
    belongs_to :school, Server.Accounts.School
    belongs_to :parent, Server.Accounts.Parent

    timestamps()
  end

  @doc false
  def changeset(child, attrs) do
    child
    |> cast(attrs, [:name, :school_id, :parent_id])
    |> validate_required([:name, :school_id])
  end

  def create_changeset(child, attrs) do
    child
    |> changeset(attrs)
    |> cast(attrs, [:egn, :rfid, :secure_code_confirmation])
    |> validate_required([:egn, :rfid, :secure_code_confirmation])
    |> unique_constraint(:egn)
    |> insert_code()
     |> secure_code()
  end

  def child_confirmation_changeset(child, attrs) do
    child
    |> changeset(attrs)
    |> validate_required([:name, :egn, :rfid])
    |> confirm(attrs)
  end

  defp confirm(%{valid?: false} = child, _), do: child

  defp confirm(
         %{valid?: true} = child,
         ##         %{secure_code_confirmation: secure_code_confirmation} =
         _attrs
       ) do
    # IO.inspect(attrs, label: "#{__MODULE__} confirm")

    # confirmation =
    #   child.name <>
    #     secure_code_confirmation <>
    #     child.egn <> secure_code_confirmation <> child.rfid <> secure_code_confirmation

    # if Hash.verify?(child.secure_code, confirmation) do
    #   put_change(child, :active?, true)
    # else
    #   child
    # end
    put_change(child, :active?, true)
  end

  defp secure_code(%{valid?: false} = child), do: child

  defp secure_code(
         %{
           valid?: true,
           changes: %{
             name: name,
             egn: egn,
             rfid: rfid,
             secure_code_confirmation: secure_code_confirmation
           }
         } = child
       ) do
    put_change(
      child,
      :secure_code,
      Hash.hash(
        name <>
          secure_code_confirmation <>
          egn <> secure_code_confirmation <> rfid <> secure_code_confirmation
      )
    )
    # put_change(child, :conf_code, secure_code_confirmation)
  end

  def insert_code(
    %{
           valid?: true,
           changes: %{
             name: name,
             egn: egn,
             rfid: rfid,
             secure_code_confirmation: secure_code_confirmation
           }
         } = child
  ) do
    put_change(child, :conf_code, secure_code_confirmation)
  end
end
