defmodule Server.Accounts.Host do
  use Ecto.Schema
  import Ecto.Changeset

  schema "hosts" do
    field :name, :string
    field :email, :string
    belongs_to :credential, Server.Accounts.Credential
    belongs_to :address, Server.Addressess.Address
    timestamps()
  end

  @doc false
  def changeset(host, attrs) do
    host
    |> cast(attrs, [:name, :email])
    |> validate_required([:name, :email])
  end
end
