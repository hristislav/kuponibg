defmodule Server.Accounts.Credential do
  use Ecto.Schema
  import Ecto.Changeset

  alias Server.Accounts.{
    Parent,
    School,
    Catering,
    Host
  }

  schema "credentials" do
    field :username, :string
    field :password_hash, :string
    field :type, :string

    field :password_old, :string, virtual: true
    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true

    has_one :parent, Parent
    has_one :school, School
    has_one :catering, Catering
    has_one :host, Host

    timestamps()
  end

  @doc false

  def changeset(credential, attrs) do
    credential
    |> cast(attrs, [:username, :type])
    |> validate_required([:username, :type])
    |> validate_length(:username, min: 8)
    |> unique_constraint(:username)
  end

  def registration_changeset(changeset, attrs \\ %{}) do
    changeset
    |> changeset(attrs)
    |> cast(attrs, [:password, :password_confirmation])
    |> validate_required([:password, :password_confirmation])
    |> validate_length(:password, min: 8)
    |> validate_confirmation(:password)
    |> hash_password()
  end

  def hash_password(%{valid?: false} = credential), do: credential

  def hash_password(%{valid?: true, changes: %{password: pass}} = credential) do
    put_change(credential, :password_hash, Hash.hash(pass))
  end
end
