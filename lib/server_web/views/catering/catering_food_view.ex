defmodule ServerWeb.CateringFoodView do
  use ServerWeb, :view
  def get_food_id(%Server.Catering.Food{id: id}), do: id

  def get_food_type_by_id(%Server.Catering.Food{food_type: id}),
    do: Server.Catering.get_food_type!(id).name
end
