defmodule ServerWeb.SessionController do
  use ServerWeb, :controller

  alias Server.{Accounts, Addressess}
  alias Server.Accounts.{Credential, Parent}
  
  def login(conn, _params) do
    cities = Addressess.list_cities(:names)
    render(conn, "login.html", cities: cities)
  end

  def create(conn, %{"registration" => %{"parent" => _parents}} = params) do
    # cred = Accounts.create_parent(params["registration"]["parent"])
    delete(conn, params)
  end

  def create(conn, %{"login" => %{"credential" => cred}} = params) do
    respo = Accounts.authenticate_account(cred["username"], cred["password"])
    IO.inspect(respo, label: " login:respo")
    # IO.inspect(cred, label: " login:cred")
    delete(conn, params)
  end

  def create(conn, _) do
    delete(conn, "")
  end

  def delete(conn, _) do
    conn
    |> configure_session(drop: true)
    |> put_flash(:success, "Logout !")
    |> redirect(to: "/")
  end
end
