defmodule ServerWeb.SchoolController do
  use ServerWeb, :controller

  alias Server.Accounts
  alias Server.Accounts.School

  def index(conn, _params) do
    schools = Accounts.list_schools()
    render(conn, "index.html", schools: schools)
  end

  def new(conn, _params) do
    cities = Server.Addressess.list_cities(:names)
    render(conn, "registration.html", cities: cities)
  end

  def create(conn, school_params) do
    case Accounts.create_school(school_params) do
      {:ok, _school_details} ->
        conn
        |> put_flash(:info, "School created successfully.")
        |> redirect(to: Routes.school_path(conn, :index))

      _ ->
        conn
        |> configure_session(drop: true)
        |> put_flash(:error, "ERROR: #{__MODULE__}.create")
        |> redirect(to: "/")
    end
  end

  def show(conn, %{"id" => id}) do
    school = Accounts.get_school!(id)
    render(conn, "show.html", school: school)
  end

  def edit(conn, %{"id" => id}) do
    school = Accounts.get_school!(id)
    changeset = Accounts.change_school(school)
    render(conn, "edit.html", school: school, changeset: changeset)
  end

  def update(conn, %{"id" => id, "school" => school_params}) do
    school = Accounts.get_school!(id)

    case Accounts.update_school(school, school_params) do
      {:ok, school} ->
        conn
        |> put_flash(:info, "School updated successfully.")
        |> redirect(to: Routes.school_path(conn, :show, school))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", school: school, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    school = Accounts.get_school!(id)
    {:ok, _school} = Accounts.delete_school(school)

    conn
    |> put_flash(:info, "School deleted successfully.")
    |> redirect(to: Routes.school_path(conn, :index))
  end
end
