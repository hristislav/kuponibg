defmodule ServerWeb.SchoolChildController do
  use ServerWeb, :controller
  alias Server.Accounts
  alias Server.Accounts.{Parent, School, Child}
  alias Server.Accounts.Child

  def index(conn, %{"school_id" => school_id} = _school_child_params) do
    school = Accounts.get_school!(school_id)

    render(conn, "index.html",
      school: %{name: school.name, id: school.id},
      children: Accounts.list_children_school_id(school.id)
    )
  end

  def new(conn, _params) do
    render(conn, "new.html", changeset: %Child{})
  end

  # def create(conn, %{"child" => child_params}) do
  #   case Accounts.create_child(child_params) do
  #     {:ok, child} ->
  #       conn
  #       |> put_flash(:info, "Child created successfully.")
  #       |> redirect(to: Routes.school_school_child_path(conn, :show, child))

  #     {:error, %Ecto.Changeset{} = changeset} ->
  #       render(conn, "new.html")
  #   end
  # end

  def create(conn, %{"add_child" => child} = params) do
    case Accounts.create_child(Map.put(child["child"], "school_id", params["school_id"])) do
      return ->
        :ok
    end
    redirect(conn, to: Routes.school_school_child_path(conn, :index, params["school_id"]))
  end

  def show(conn, %{"id" => id}) do
    child = Accounts.get_child!(id)
    render(conn, "show.html", child: child)
  end

  def edit(conn, %{"id" => id}) do
    child = Accounts.get_child!(id)
    changeset = Accounts.change_child(child)
    render(conn, "edit.html", child: child, changeset: changeset)
  end

  def update(conn, %{"id" => id, "child" => child_params}) do
    child = Accounts.get_child!(id)

    case Accounts.update_child(child, child_params) do
      {:ok, child} ->
        conn
        |> put_flash(:info, "Child updated successfully.")
        |> redirect(to: Routes.school_school_child_path(conn, :show, child))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", child: child, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    child = Accounts.get_child!(id)
    {:ok, _child} = Accounts.delete_child(child)

    conn
    |> put_flash(:info, "Child deleted successfully.")
    # TODO redirect to index children
    |> redirect(to: Routes.school_path(conn, :index))
  end
end
