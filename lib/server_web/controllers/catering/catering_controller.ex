defmodule ServerWeb.CateringController do
  use ServerWeb, :controller

  alias Server.Accounts
  alias Server.Accounts.Catering
  alias Server.Addressess
  alias Server.Repo
  alias Server.Accounts.Credential

  def index(conn, _params) do
    conn
    |> assign(:cities, Addressess.list_cities(:names))
    |> assign(:caterings, Accounts.list_caterings())
    |> render("index.html")
  end

  def new(conn, _params) do
    # Print.term(params, "#{__MODULE__}.new/params")
    conn
    |> assign(:cities, Addressess.list_cities(:names))
    |> assign(:changeset, Accounts.change_catering())
    |> render("new.html")
  end

  def create(conn, %{"catering" => catering_params} = _params) do
    # Print.term(params, "#{__MODULE__}.create/params")
    IO.inspect(catering_params, label: "catering =>")

    case Accounts.create_catering(catering_params) do
      {:ok, %Catering{} = catering} ->
        conn
        |> put_flash(:info, "Successfull : #{__MODULE__}.create")
        |> redirect(to: Routes.catering_path(conn, :show, catering))

      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        # |> configure_session(drop: true)
        |> put_flash(:error, "ERROR: #{__MODULE__}.create")
        |> assign(:cities, Addressess.list_cities(:names))
        |> assign(:changeset, changeset)
        |> render("new.html")
    end
  end

  def delete(conn, %{"id" => id} = params) do
    Print.term(params, "#{__MODULE__}.delete/params")

    {:ok, _catering} =
      id
      |> Accounts.get_catering!()
      |> Accounts.delete_catering()

    conn
    |> put_flash(:info, "Catering deleted successfully.")
    |> redirect(to: Routes.catering_path(conn, :index))
  end

  def show(conn, %{"id" => id} = params) do
    # Print.term(params, "#{__MODULE__}.show/params")

    conn
    |> assign(:catering, Accounts.get_catering!(id))
    |> render("show.html")
  end

  def edit(conn, %{"id" => id} = params) do
    # Print.term(params, "#{__MODULE__}.edit/params")
    catering =
      Accounts.get_catering!(id)
      |> Repo.preload(:address)

    Print.term(catering, "#{__MODULE__}.edit/catering")

    assign = [
      catering: catering,
      changeset: Accounts.change_catering(catering),
      cities: Addressess.list_cities(:names)
    ]

    render(conn, "edit.html", assign)
  end

  def update(conn, %{"id" => id, "catering" => catering_params} = params) do
    # Print.term(params, "#{__MODULE__}.update/params")
    catering =
      id
      |> Accounts.get_catering!()
      |> Repo.preload(:address)

    Print.term(catering, "#{__MODULE__}.update/catering")

    case Accounts.update_catering(catering, catering_params) do
      {:ok, updated_catering} ->
        new_assign_conn =
          conn
          |> assign(:catering, updated_catering)
          |> assign(:changeset, updated_catering)
          |> assign(:cities, Addressess.list_cities(:names))

        new_assign_conn
        |> put_flash(:info, "Catering updated successfully.")
        |> redirect(to: Routes.catering_path(new_assign_conn, :show, catering))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", catering: catering, changeset: changeset)
    end
  end

  defp parse_cities(cities) do
    Enum.map(cities, fn %Server.Addressess.Address{id: id, city: name} -> {name, id} end)
  end
end
