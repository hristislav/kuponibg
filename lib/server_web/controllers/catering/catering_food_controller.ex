defmodule ServerWeb.CateringFoodController do
  use ServerWeb, :controller

  alias Server.{Repo, Accounts}
  alias Server.Catering, as: Catering
  alias Catering.Food, as: Food
  alias Catering.FoodType, as: FoodType

  def index(conn, %{"catering_id" => cid}) do
    %Server.Accounts.Catering{foods: foods} =
      Accounts.get_catering!(cid)
      |> Repo.preload(:foods)
      
    render(conn, "index.html",
      catering_id: cid,
      foods: foods
    )
  end

  def new(conn, params) do
    food = Catering.change_food(%Food{})

    render(conn, "new.html",
      catering_id: params["catering_id"],
      food: food,
      food_types: parse_food_types(Catering.list_food_types())
    )
  end

  def create(conn, %{"food" => food_params, "catering_id" => cid}) do
    case Catering.create_food(Map.put(food_params, "catering_id", cid)) do
      {:ok, food_id} ->
        conn
        |> put_flash(:info, "Food created successfully.")
        |> redirect(to: Routes.catering_catering_food_path(conn, :show, cid, food_id))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html",
          catering_id: cid,
          food: changeset,
          food_types: parse_food_types(Server.Catering.list_food_types())
        )
    end
  end

  def show(conn, %{"id" => id, "catering_id" => cid}) do
    food = Server.Catering.get_food!(id)
    render(conn, "show.html", catering_id: cid, food: food)
  end

  def edit(conn, %{"id" => food_id, "catering_id" => cid}) do
    changeset = food_id |> Server.Catering.get_food!() |> Server.Catering.change_food()

    render(conn, "edit.html",
      catering_id: cid,
      id: food_id,
      food: changeset,
      food_types: parse_food_types(Server.Catering.list_food_types())
    )
  end

  def update(conn, %{"id" => food_id, "catering_id" => cid, "food" => food_params}) do
    food = Server.Catering.get_food!(food_id)

    case Server.Catering.update_food(food, food_params) do
      {:ok, _} ->
        conn
        |> put_flash(:info, "Food updated successfully.")
        |> redirect(to: Routes.catering_catering_food_path(conn, :show, cid, food_id))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html",
          catering_id: cid,
          food: changeset,
          food_types: parse_food_types(Server.Catering.list_food_types())
        )
    end
  end

  def delete(conn, %{"id" => id, "catering_id" => cid}) do
    {:ok, _} =
      id
      |> Server.Catering.get_food!()
      |> Server.Catering.delete_food()

    conn
    |> put_flash(:info, "Food deleted successfully.")
    |> redirect(to: Routes.catering_catering_food_path(conn, :index, cid))
  end

  defp parse_food_types(food_types) do
    Enum.map(food_types, fn %FoodType{name: name, id: id} -> {name, id} end)
  end
end
