defmodule ServerWeb.CateringController1 do
  use ServerWeb, :controller

  alias Server.{Accounts}
  # alias Print

  def index(conn, params) do
    IO.inspect(params, label: "#{__MODULE__}.index/params")
    caterings = Accounts.list_caterings()
    render(conn, "index.html", caterings: [])
  end

  def new(conn, _params) do
    cities = Server.Addressess.list_cities(:names)
    render(conn, "registration.html", cities: cities)
  end

  # def create(conn, catering_params) do
  #   # cred = Accounts.create_parent(params["registration"]["parent"])
  #   IO.inspect(catering_params, label: "CREATE:catering_params")

  #   # IO.inspect(params, label: " create:params")
  #   delete(conn, catering_params)
  # end

  def create(conn, params) do
    catering_params =
      params
      |> get_in(~w(registration catering)s)
      |> put_in(~w(credential type)s, "catering")

    case Accounts.create_catering(catering_params) do
      {:ok, _catering_details} ->
        conn
        |> put_flash(:info, "Catering created successfully.")
        |> redirect(to: Routes.catering_path(conn, :index))

      _error_message ->
        conn
        |> configure_session(drop: true)
        |> put_flash(:war, "ERROR: #{__MODULE__}.create")
        |> redirect(to: Routes.catering_path(conn, :index))
    end
  end

  def delete(conn, _) do
    conn
    |> configure_session(drop: true)
    |> put_flash(:success, "Logout !")
    |> redirect(to: "/")
  end

  # def create(conn, %{"catering" => catering_params}) do
  #   case Accounts.create_catering(catering_params) do
  #     {:ok, catering} ->
  #       conn
  #       |> put_flash(:info, "Catering created successfully.")
  #       |> redirect(to: Routes.catering_path(conn, :show, catering))

  #     {:error, %Ecto.Changeset{} = changeset} ->
  #       render(conn, "new.html", changeset: changeset)
  #   end
  # end

  def show(conn, %{"id" => id}) do
    catering = Accounts.get_catering!(id)
    render(conn, "show.html", catering: catering)
  end

  def edit(conn, %{"id" => id}) do
    catering = Accounts.get_catering!(id)
    changeset = Accounts.change_catering(catering)
    render(conn, "edit.html", catering: catering, changeset: changeset)
  end

  def update(conn, %{"id" => id, "catering" => catering_params}) do
    catering = Accounts.get_catering!(id)

    case Accounts.update_catering(catering, catering_params) do
      {:ok, catering} ->
        conn
        |> put_flash(:info, "Catering updated successfully.")
        |> redirect(to: Routes.catering_path(conn, :show, catering))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", catering: catering, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    catering = Accounts.get_catering!(id)
    {:ok, _catering} = Accounts.delete_catering(catering)

    conn
    |> put_flash(:info, "Catering deleted successfully.")
    |> redirect(to: Routes.catering_path(conn, :index))
  end
end
