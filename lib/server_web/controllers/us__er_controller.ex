defmodule ServerWeb.Us__erController do
  use ServerWeb, :controller

  alias Server.Ac__counts
  alias Server.Ac__counts.Us__er

  def index(conn, _params) do
    us__ers = Ac__counts.list_us__ers()
    render(conn, "index.html", us__ers: us__ers)
  end

  def new(conn, _params) do
    changeset = Ac__counts.change_us__er(%Us__er{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"us__er" => us__er_params}) do
    case Ac__counts.create_us__er(us__er_params) do
      {:ok, us__er} ->
        conn
        |> put_flash(:info, "Us  er created successfully.")
        |> redirect(to: Routes.us__er_path(conn, :show, us__er))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    us__er = Ac__counts.get_us__er!(id)
    render(conn, "show.html", us__er: us__er)
  end

  def edit(conn, %{"id" => id}) do
    us__er = Ac__counts.get_us__er!(id)
    changeset = Ac__counts.change_us__er(us__er)
    render(conn, "edit.html", us__er: us__er, changeset: changeset)
  end

  def update(conn, %{"id" => id, "us__er" => us__er_params}) do
    us__er = Ac__counts.get_us__er!(id)

    case Ac__counts.update_us__er(us__er, us__er_params) do
      {:ok, us__er} ->
        conn
        |> put_flash(:info, "Us  er updated successfully.")
        |> redirect(to: Routes.us__er_path(conn, :show, us__er))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", us__er: us__er, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    us__er = Ac__counts.get_us__er!(id)
    {:ok, _us__er} = Ac__counts.delete_us__er(us__er)

    conn
    |> put_flash(:info, "Us  er deleted successfully.")
    |> redirect(to: Routes.us__er_path(conn, :index))
  end
end
