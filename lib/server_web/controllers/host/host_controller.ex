defmodule ServerWeb.HostController do
  use ServerWeb, :controller

  alias Server.Accounts
  alias Server.Accounts.Host

  def index(conn, _params) do
    hosts = Accounts.list_hosts()
    render(conn, "index.html", hosts: hosts)
  end

  def new(conn, _params) do
    cities = Server.Addressess.list_cities(:names)

    render(conn, "registration.html", cities: cities)
  end

  # def create(conn, %{"host" => host_params}) do
  def create(conn, params) do
    # host_params =
    #   params
    #   |> get_in(~w(registration host)s)
    #   |> put_in(~w(credential type)s, "host")

    case Accounts.create_host(params["registration"]) do
      {:ok, _host_details} ->
        conn
        |> put_flash(:info, "Host created successfully.")
        |> redirect(to: Routes.host_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    host = Accounts.get_host!(id)
    render(conn, "show.html", host: host)
  end

  def edit(conn, %{"id" => id}) do
    host = Accounts.get_host!(id)
    changeset = Accounts.change_host(host)
    render(conn, "edit.html", host: host, changeset: changeset)
  end

  def update(conn, %{"id" => id, "host" => host_params}) do
    host = Accounts.get_host!(id)

    case Accounts.update_host(host, host_params) do
      {:ok, host} ->
        conn
        |> put_flash(:info, "Host updated successfully.")
        |> redirect(to: Routes.host_path(conn, :show, host))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", host: host, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    host = Accounts.get_host!(id)
    {:ok, _host} = Accounts.delete_host(host)

    conn
    |> put_flash(:info, "Host deleted successfully.")
    |> redirect(to: Routes.host_path(conn, :index))
  end
end
