defmodule ServerWeb.ParentController do
  use ServerWeb, :controller

  alias Server.Accounts
  alias Server.Accounts.Parent

  def index(conn, _params) do
    parents = Accounts.list_parents()
    render(conn, "index.html", parents: parents)
  end

  def new(conn, _params) do
    cities = Server.Addressess.list_cities(:names)

    render(conn, "registration.html", cities: cities)
  end

  # def create(conn, %{"parent" => parent_params}) do
  def create(conn, params) do
  IO.inspect params, label: "Params =>"
    # Print.term(params, "#{__MODULE__}.create/params")

    parent_params =
      params
      |> get_in(~w(registration parent)s)
      |> put_in(~w(credential type)s, "parent")

    # Print.term(parent_params, "#{__MODULE__}.create/parent_params")

    case Accounts.create_parent(parent_params) do
      {:ok, _parent_details} ->
        conn
        |> put_flash(:info, "Parent created successfully.")
        |> redirect(to: Routes.parent_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        # Print.term(changeset, "#{__MODULE__}.create/error/changeset")
        IO.puts "error"
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    parent = Accounts.get_parent!(id)
    render(conn, "show.html", parent: parent)
  end

  def edit(conn, %{"id" => id}) do
    parent = Accounts.get_parent!(id)
    changeset = Accounts.change_parent(parent)
    render(conn, "edit.html", parent: parent, changeset: changeset)
  end

  def update(conn, %{"id" => id, "parent" => parent_params}) do
    parent = Accounts.get_parent!(id)

    case Accounts.update_parent(parent, parent_params) do
      {:ok, parent} ->
        conn
        |> put_flash(:info, "Parent updated successfully.")
        |> redirect(to: Routes.parent_path(conn, :show, parent))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", parent: parent, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    parent = Accounts.get_parent!(id)
    {:ok, _parent} = Accounts.delete_parent(parent)

    conn
    |> put_flash(:info, "Parent deleted successfully.")
    |> redirect(to: Routes.parent_path(conn, :index))
  end

  def registration(conn, _) do
    cities = Server.Addressess.list_cities(:names)
    render(conn, "registration.html", cities: cities)
  end
end
