defmodule ServerWeb.ParentChildController do
  use ServerWeb, :controller

  alias Server.{Repo, Accounts}
  alias Server.Accounts.{Child}
  # alias Server.Accounts.{Parent, School, Child}

  def index(conn, %{"parent_id" => parent_id} = _parent_child_params) do
    %{children: children} =
      parent_id
      |> Accounts.get_parent!()
      |> Repo.preload(:children)

    IO.inspect(children, label: "parent/children")

    conn
    |> render("index.html",
      children: children |> Repo.preload(:school),
      parent_id: parent_id
    )
  end

  def new(conn, _params) do
    changeset = Accounts.change_child(%Child{})

    conn
    |> render("new.html", changeset: changeset)
  end

  def create(conn, %{"child_configmation" => child_configmation} = params) do
    confirm_params =
      Map.put(child_configmation["child_configmation"], "parent_id", params["parent_id"])

    # IO.inspect(confirm_params, label: "#{__MODULE__}.confirm_params")

    Accounts.child_configmation(confirm_params)
    |> IO.inspect(label: "#{__MODULE__}.child_configmation")

    index(conn, params)
    # case Accounts.create_child(child_params) do
    #   {:ok, child} ->
    #     conn
    #     |> put_flash(:info, "Child created successfully.")
    #     |> redirect(to: Routes.child_path(conn, :show, child))
    #
    #   {:error, %Ecto.Changeset{} = changeset} ->
    #     render(conn, "new.html", changeset: changeset)
    # end
  end

  def show(conn, %{"id" => id} = _params) do
    child = Accounts.get_child!(id)
    render(conn, "show.html", child: child)
  end

  def edit(conn, %{"id" => id}) do
    child = Accounts.get_child!(id)
    render(conn, "edit.html", child: child)
  end

  def update(conn, %{"id" => id, "child" => child_params}) do
    child = Accounts.get_child!(id)

    case Accounts.update_child(child, child_params) do
      {:ok, child} ->
        conn
        |> put_flash(:info, "Child updated successfully.")
        |> redirect(to: Routes.parent_parent_child_path(conn, :show, child))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", child: child, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    child = Accounts.get_child!(id)
    {:ok, _child} = Accounts.delete_child(child)

    conn
    |> put_flash(:info, "Child deleted successfully.")
    |> redirect(to: Routes.parent_path(conn, :index))
  end
end
