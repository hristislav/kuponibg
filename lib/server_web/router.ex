defmodule ServerWeb.Router do
  use ServerWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  # pipeline :api do
  #   plug :accepts, ["json"]
  # end

  scope "/", ServerWeb do
    pipe_through :browser

    get "/", PageController, :index
    resources "/us__ers", Us__erController

    get "/login", SessionController, :login
    get "/register", ParentController, :registration
    get "/logout", SessionController, :delete
    resources("/sessions", SessionController, only: [:new, :create, :delete], singleton: true)

    resources "/administrators", AdministratorController

    resources "/parents", ParentController do
      resources "/wallet", ParentWalletController

      resources "/children", ParentChildController do
      end

      resources "/orders", ParentOrderController
    end

    resources "/caterings", CateringController do
      resources "/orders", CateringOrderController
      resources "/menus", CateringMenuController
      resources "/foods", CateringFoodController
    end

    resources "/schools", SchoolController do
      resources "/children", SchoolChildController
    end

    resources "/hosts", HostController
  end

  # Other scopes may use custom stacks.
  # scope "/api", ServerWeb do
  #   pipe_through :api
  # end
end
