defmodule Server.Ac__countsTest do
  use Server.DataCase

  alias Server.Ac__counts

  describe "us__ers" do
    alias Server.Ac__counts.Us__er

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def us__er_fixture(attrs \\ %{}) do
      {:ok, us__er} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Ac__counts.create_us__er()

      us__er
    end

    test "list_us__ers/0 returns all us__ers" do
      us__er = us__er_fixture()
      assert Ac__counts.list_us__ers() == [us__er]
    end

    test "get_us__er!/1 returns the us__er with given id" do
      us__er = us__er_fixture()
      assert Ac__counts.get_us__er!(us__er.id) == us__er
    end

    test "create_us__er/1 with valid data creates a us__er" do
      assert {:ok, %Us__er{} = us__er} = Ac__counts.create_us__er(@valid_attrs)
      assert us__er.name == "some name"
    end

    test "create_us__er/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Ac__counts.create_us__er(@invalid_attrs)
    end

    test "update_us__er/2 with valid data updates the us__er" do
      us__er = us__er_fixture()
      assert {:ok, %Us__er{} = us__er} = Ac__counts.update_us__er(us__er, @update_attrs)
      assert us__er.name == "some updated name"
    end

    test "update_us__er/2 with invalid data returns error changeset" do
      us__er = us__er_fixture()
      assert {:error, %Ecto.Changeset{}} = Ac__counts.update_us__er(us__er, @invalid_attrs)
      assert us__er == Ac__counts.get_us__er!(us__er.id)
    end

    test "delete_us__er/1 deletes the us__er" do
      us__er = us__er_fixture()
      assert {:ok, %Us__er{}} = Ac__counts.delete_us__er(us__er)
      assert_raise Ecto.NoResultsError, fn -> Ac__counts.get_us__er!(us__er.id) end
    end

    test "change_us__er/1 returns a us__er changeset" do
      us__er = us__er_fixture()
      assert %Ecto.Changeset{} = Ac__counts.change_us__er(us__er)
    end
  end
end
