defmodule ServerWeb.CateringControllerTest do
  use ServerWeb.ConnCase

  alias Server.Accounts

  @create_attrs %{name: "some name"}
  @update_attrs %{name: "some updated name"}
  @invalid_attrs %{name: nil}

  def fixture(:catering) do
    {:ok, catering} = Accounts.create_catering(@create_attrs)
    catering
  end

  describe "index" do
    test "lists all caterings", %{conn: conn} do
      conn = get(conn, Routes.catering_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Caterings"
    end
  end

  describe "new catering" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.catering_path(conn, :new))
      assert html_response(conn, 200) =~ "New Catering"
    end
  end

  describe "create catering" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.catering_path(conn, :create), catering: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.catering_path(conn, :show, id)

      conn = get(conn, Routes.catering_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Catering"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.catering_path(conn, :create), catering: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Catering"
    end
  end

  describe "edit catering" do
    setup [:create_catering]

    test "renders form for editing chosen catering", %{conn: conn, catering: catering} do
      conn = get(conn, Routes.catering_path(conn, :edit, catering))
      assert html_response(conn, 200) =~ "Edit Catering"
    end
  end

  describe "update catering" do
    setup [:create_catering]

    test "redirects when data is valid", %{conn: conn, catering: catering} do
      conn = put(conn, Routes.catering_path(conn, :update, catering), catering: @update_attrs)
      assert redirected_to(conn) == Routes.catering_path(conn, :show, catering)

      conn = get(conn, Routes.catering_path(conn, :show, catering))
      assert html_response(conn, 200) =~ "some updated name"
    end

    test "renders errors when data is invalid", %{conn: conn, catering: catering} do
      conn = put(conn, Routes.catering_path(conn, :update, catering), catering: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Catering"
    end
  end

  describe "delete catering" do
    setup [:create_catering]

    test "deletes chosen catering", %{conn: conn, catering: catering} do
      conn = delete(conn, Routes.catering_path(conn, :delete, catering))
      assert redirected_to(conn) == Routes.catering_path(conn, :index)

      assert_error_sent 404, fn ->
        get(conn, Routes.catering_path(conn, :show, catering))
      end
    end
  end

  defp create_catering(_) do
    catering = fixture(:catering)
    {:ok, catering: catering}
  end
end
