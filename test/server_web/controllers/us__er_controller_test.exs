defmodule ServerWeb.Us__erControllerTest do
  use ServerWeb.ConnCase

  alias Server.Ac__counts

  @create_attrs %{name: "some name"}
  @update_attrs %{name: "some updated name"}
  @invalid_attrs %{name: nil}

  def fixture(:us__er) do
    {:ok, us__er} = Ac__counts.create_us__er(@create_attrs)
    us__er
  end

  describe "index" do
    test "lists all us__ers", %{conn: conn} do
      conn = get(conn, Routes.us__er_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Us  ers"
    end
  end

  describe "new us__er" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.us__er_path(conn, :new))
      assert html_response(conn, 200) =~ "New Us  er"
    end
  end

  describe "create us__er" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.us__er_path(conn, :create), us__er: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.us__er_path(conn, :show, id)

      conn = get(conn, Routes.us__er_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Us  er"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.us__er_path(conn, :create), us__er: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Us  er"
    end
  end

  describe "edit us__er" do
    setup [:create_us__er]

    test "renders form for editing chosen us__er", %{conn: conn, us__er: us__er} do
      conn = get(conn, Routes.us__er_path(conn, :edit, us__er))
      assert html_response(conn, 200) =~ "Edit Us  er"
    end
  end

  describe "update us__er" do
    setup [:create_us__er]

    test "redirects when data is valid", %{conn: conn, us__er: us__er} do
      conn = put(conn, Routes.us__er_path(conn, :update, us__er), us__er: @update_attrs)
      assert redirected_to(conn) == Routes.us__er_path(conn, :show, us__er)

      conn = get(conn, Routes.us__er_path(conn, :show, us__er))
      assert html_response(conn, 200) =~ "some updated name"
    end

    test "renders errors when data is invalid", %{conn: conn, us__er: us__er} do
      conn = put(conn, Routes.us__er_path(conn, :update, us__er), us__er: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Us  er"
    end
  end

  describe "delete us__er" do
    setup [:create_us__er]

    test "deletes chosen us__er", %{conn: conn, us__er: us__er} do
      conn = delete(conn, Routes.us__er_path(conn, :delete, us__er))
      assert redirected_to(conn) == Routes.us__er_path(conn, :index)
      assert_error_sent 404, fn ->
        get(conn, Routes.us__er_path(conn, :show, us__er))
      end
    end
  end

  defp create_us__er(_) do
    us__er = fixture(:us__er)
    {:ok, us__er: us__er}
  end
end
