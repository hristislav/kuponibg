defmodule Server.Repo.Migrations.CreateUsErs do
  use Ecto.Migration

  def change do
    create table(:us__ers) do
      add :name, :string

      timestamps()
    end

  end
end
