defmodule Server.Repo.Migrations.CreateCaterings do
  use Ecto.Migration

  def change do
    create table(:caterings) do
      add :name, :string
      add :email, :string
      add :credential_id, references(:credentials)
      add :address_id, references(:addressess)
      timestamps()
    end
  end
end
