defmodule Server.Repo.Migrations.CreateParents do
  use Ecto.Migration

  def change do
    create table(:parents) do
      add :name, :string
      add :email, :string
      add :credential_id, references(:credentials)
      add :address_id, references(:addressess)
      timestamps()
    end
  end
end
