defmodule Server.Repo.Migrations.CreateAdministrators do
  use Ecto.Migration

  def change do
    create table(:administrators) do
      add :name, :string
      # add :address_id, references(:addressess)
      timestamps()
    end
  end
end
