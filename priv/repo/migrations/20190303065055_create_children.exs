defmodule Server.Repo.Migrations.CreateChildren do
  use Ecto.Migration

  def change do
    create table(:children) do
      add(:name, :string)
      add(:rfid, :string)
      add(:secure_code, :string)
      add(:egn, :string)
      add(:active?, :boolean)
      add(:conf_code, :string)
      add(:parent_id, references(:parents))
      add(:school_id, references(:schools))
      # add :address_id, references(:addressess)
      timestamps()
    end

    create(unique_index(:children, [:egn]))
  end
end
